module gitlab.com/yakshaving.art/back-to-slack

go 1.13

require (
	github.com/xanzy/go-gitlab v0.22.1
	gopkg.in/yaml.v2 v2.2.7
)

replace github.com/xanzy/go-gitlab => github.com/pcarranza/go-gitlab v0.22.4
